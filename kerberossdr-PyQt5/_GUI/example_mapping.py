import geotiler
map = geotiler.Map(center=(-120.66453, 35.30272), zoom=19, size=(512, 512))
map.extent
image = geotiler.render_map(map)
image.save('map.png')