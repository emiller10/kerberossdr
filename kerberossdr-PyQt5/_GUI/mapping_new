# KerberosSDR Python GUI

# Copyright (C) 2018-2019  Carl Laufer, Tamás Pető
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# -*- coding: utf-8 -*

import sys
import os
import time
import math
import pyqtgraph as pg
import pyqtgraph.exporters
import numpy as np
import scipy
from bottle import route, run, request, get, post, redirect, template, static_file
import threading
import subprocess

np.seterr(divide='ignore')

# Import Kerberos modules
currentPath = os.path.dirname(os.path.realpath(__file__))
rootPath = os.path.dirname(currentPath)

webDisplayPath      = os.path.join(rootPath, "_webDisplay")

sys.path.insert(0, webDisplayPath)

# Import graphical user interface packages --Modified to use PyQt5
from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import *
from PyQt5.QtGui import *

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

# Import packages for plotting
import matplotlib
matplotlib.use('Agg') # For Raspberry Pi compatiblity
from matplotlib import cm
#from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
#from matplotlib.backends.backend_qt4 import NavigationToolbar2QT as NavigationToolbar
#import matplotlib.pyplot as plt
#import matplotlib.patches as patches

from mapping_main_window_layout import Ui_MainWindow

# Import the pyArgus module
#root_path = os.getcwd()
#pyargus_path = os.path.join(os.path.join(root_path, "pyArgus"), "pyArgus")
#sys.path.insert(0, pyargus_path)
#import directionEstimation as de

from pyargus import directionEstimation as de

class MainWindow(QMainWindow, Ui_MainWindow):

    def __init__ (self,parent = None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        #f = open('/dev/null', 'w')
        #sys.stdout = f

        self.tabWidget.setCurrentIndex(0)

        # Set pyqtgraph to use white background, black foreground
        pg.setConfigOption('background', (61, 61, 61))
        pg.setConfigOption('foreground', 'w')
        pg.setConfigOption('imageAxisOrder', 'row-major')
        #pg.setConfigOption('useOpenGL', True)
        #pg.setConfigOption('useWeave', True)

        # Spectrum display

        self.win_mapping = pg.GraphicsWindow(title="GPS Mapping")

        self.export_mapping = pg.exporters.ImageExporter(self.win_mapping.scene())
        
        
        self.plotWidget_DOA = self.win_mapping.addPlot(title="Direction of Arrival Estimation")
        self.plotWidget_DOA.setLabel("bottom", "Incident Angle [deg]")
        self.plotWidget_DOA.setLabel("left", "Amplitude [dB]")
        self.plotWidget_DOA.showGrid(x=True, alpha=0.25)
        self.gridLayout_RD.addWidget(self.win_mapping, 1, 1, 1, 1)
        
        x = np.arange(1000)
        y = np.random.normal(size=(4,1000))

        self.plotWidget_DOA.addLegend()

        self.plotWidget_DOA.plot(x, y[0], pen=pg.mkPen((255, 199, 15), width=2), name="Bartlett")
        self.plotWidget_DOA.plot(x, y[1], pen=pg.mkPen('g', width=2), name="Capon")
        self.plotWidget_DOA.plot(x, y[2], pen=pg.mkPen('r', width=2), name="MEM")
        self.plotWidget_DOA.plot(x, y[3], pen=pg.mkPen((9, 237, 237), width=2), name="MUSIC")
        
        # Open file that has the DOA and confidence interval
        self.DOA_res_fd = open("/ram/DOA_value.html","w")
        

        # Connect window buttons and things signals
        self.checkBox_en_mapping.stateChanged.connect(self.set_map_params)
        self.doubleSpinBox_latitude.valueChanged.connect(self.set_map_params)
        self.doubleSpinBox_longitude.valueChanged.connect(self.set_map_params)
        self.doubleSpinBox_latitude_beacon.valueChanged.connect(self.set_map_params)
        self.doubleSpinBox_longitude_beacon.valueChanged.connect(self.set_map_params)
        
        
      
        
    def set_map_params(self):
    	self.x = 0

    def set_default_configuration(self):
        self.checkBox_en_mapping.setChecked(False)
        

    def tab_changed(self):
        tab_index = self.tabWidget.currentIndex()

        if tab_index == 0:  # Spectrum tab
            self.stackedWidget_config.setCurrentIndex(0)
        elif tab_index == 1:  # Sync tab
            self.stackedWidget_config.setCurrentIndex(1)
        elif tab_index == 2:  # DOA tab
            self.stackedWidget_config.setCurrentIndex(2)
        elif tab_index == 3:  # PR tab
            self.stackedWidget_config.setCurrentIndex(3)

   

#-----------------------------------------------------------------
#
#-----------------------------------------------------------------
	


app = QApplication(sys.argv)
form = MainWindow()
form.show()


app.exec_()