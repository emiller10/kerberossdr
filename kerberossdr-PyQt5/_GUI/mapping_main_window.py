# KerberosSDR Python GUI

# Copyright (C) 2018-2019  Carl Laufer, Tamás Pető
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# -*- coding: utf-8 -*

import sys
import os
import time
import math
import pyqtgraph as pg
import pyqtgraph.exporters
import numpy as np
import scipy
from bottle import route, run, request, get, post, redirect, template, static_file
import threading
import subprocess
import geotiler
import random

np.seterr(divide='ignore')

# Import Kerberos modules
currentPath = os.path.dirname(os.path.realpath(__file__))
rootPath = os.path.dirname(currentPath)

webDisplayPath      = os.path.join(rootPath, "_webDisplay")

sys.path.insert(0, webDisplayPath)

# Import graphical user interface packages --Modified to use PyQt5
from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import *
from PyQt5.QtGui import *

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

# Import packages for plotting
import matplotlib
matplotlib.use('Agg') # For Raspberry Pi compatiblity
from matplotlib import cm
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5 import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
#import matplotlib.patches as patches

from mapping_main_window_layout import Ui_MainWindow
# from hydra_main_window.py import MainWindow

# Import the pyArgus module
#root_path = os.getcwd()
#pyargus_path = os.path.join(os.path.join(root_path, "pyArgus"), "pyArgus")
#sys.path.insert(0, pyargus_path)
#import directionEstimation as de

from pyargus import directionEstimation as de

class MainWindow(QMainWindow, Ui_MainWindow):

    def __init__ (self,parent = None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        #f = open('/dev/null', 'w')
        #sys.stdout = f

        self.tabWidget.setCurrentIndex(0)

        # Set pyqtgraph to use white background, black foreground
        pg.setConfigOption('background', (61, 61, 61))
        pg.setConfigOption('foreground', 'w')
        pg.setConfigOption('imageAxisOrder', 'row-major')
        #pg.setConfigOption('useOpenGL', True)
        #pg.setConfigOption('useWeave', True)

        # Spectrum display

        # self.win_mapping = pg.GraphicsWindow(title="GPS Mapping")

        # self.export_mapping = pg.exporters.ImageExporter(self.win_mapping.scene())
        
        
        # self.plotWidget_DOA = self.win_mapping.addPlot(title="Mapping of SAVER Location")
        self.i = 0
        
        x = np.arange(1000)
        y = np.random.normal(size=(4,1000))
        
        self.figure = Figure()
        
        self.canvas = FigureCanvas(self.figure)
        self.gridLayout_RD.addWidget(self.canvas, 1, 1, 1, 1)
        
       
        # self.figure.plot(x, y[0]) #, pen=pg.mkPen((255, 199, 15), width=2), name="Current Bearing")
       
        
        # self.plotWidget_DOA.addLegend()
        
        
        
        # self.plotWidget_DOA.plot(x, y[1], pen=pg.mkPen('g', width=2), name="Past Bearings")
        # self.plotWidget_DOA.plot(x, y[2], pen=pg.mkPen('r', width=2), name="Potential Beacon Location")
        # self.plotWidget_DOA.plot(x, y[3], pen=pg.mkPen((9, 237, 237), width=2), name="Actual Beacon Location")
        
        # Open file that has the DOA and confidence interval
        # self.DOA_res_fd = open("/ram/DOA_value.html","w")
        

        # Connect window buttons and things signals
        self.checkBox_en_mapping.stateChanged.connect(self.en_mapping_fun)
        
        self.doubleSpinBox_latitude.valueChanged.connect(self.set_map_params)
        self.doubleSpinBox_longitude.valueChanged.connect(self.set_map_params)
        self.doubleSpinBox_latitude_beacon.valueChanged.connect(self.set_map_params)
        self.doubleSpinBox_longitude_beacon.valueChanged.connect(self.set_map_params)
        self.doubleSpinBox_latitude_new.valueChanged.connect(self.set_map_params)
        self.doubleSpinBox_longitude_new.valueChanged.connect(self.set_map_params)
        
        self.pushButton_recenter.clicked.connect(self.map_recenter)
        self.pushButton_update_beacon.clicked.connect(self.plot)
        self.pushButton_run.clicked.connect(self.run)
        self.initial = True
        
        self.doa = [0,0,0]
        self.total_data_points = [0]
        self.data_index = 0
        self.intersect = [0,0,0]
        self.set_default_configuration()
        
        # random data
        self.x = [self.longitude,self.longitude,self.longitude]
        self.y = [self.latitude,self.latitude,self.latitude]
        for j in range(0,3):
            self.x[j] = [self.longitude, self.longitude+np.sin(self.doa[j]*(2*np.pi/360))]
            self.y[j] = [self.latitude, self.latitude+np.cos(self.doa[j]*(2*np.pi/360))]
        print(self.x)
        print(self.y)
        # self.run()
      
    def plot(self):
        ''' plot some random stuff '''
        
        # create an axis
        ax = self.figure.add_subplot(111)
        self.x[self.i] = [self.longitude, self.longitude+np.sin(self.doa[self.i]*(2*np.pi/360))]
        self.y[self.i] = [self.latitude, self.latitude+np.cos(self.doa[self.i]*(2*np.pi/360))]
        # discards the old graph
        ax.clear()
        print(self.x)
        print(self.y)
        
        self.intersect[0] = intersectLines([self.x[0][0],self.y[0][0]],[self.x[0][1],self.y[0][1]],[self.x[1][0],self.y[1][0]],[self.x[1][1],self.y[1][1]] )
        self.intersect[1] = intersectLines([self.x[1][0],self.y[1][0]],[self.x[1][1],self.y[1][1]],[self.x[2][0],self.y[2][0]],[self.x[2][1],self.y[2][1]] )
        self.intersect[2] = intersectLines([self.x[0][0],self.y[0][0]],[self.x[0][1],self.y[0][1]],[self.x[2][0],self.y[2][0]],[self.x[2][1],self.y[2][1]] )
        print(self.intersect)
        
        # plot data
        for j in range(0,3):
            ax.plot(self.x[j],self.y[j], '-')
            ax.plot(self.intersect[j],'.')
            
       
        ax.plot(self.beacon_long,self.beacon_lat, '.')
        
        plt.xlim([self.map_bounds[0], self.map_bounds[2]])
        plt.ylim([self.map_bounds[1], self.map_bounds[3]])
        ax.axis('off')
        
        img = plt.imread("map.png")
        ax.imshow(img, extent=[self.map_bounds[0], self.map_bounds[2],self.map_bounds[1], self.map_bounds[3]])

        # refresh canvas
        self.canvas.draw() 
    
    def DOA_update(self, doa_data):
        
        doa_text = "<span style=\" font-size:8pt; font-weight:600; color:#ff0000;\" >"
        doa_text += str(doa_data[0])
        doa_text += ("</span>")
        self.DOA_value.setText(doa_text)
        
        con_text = "<span style=\" font-size:8pt; font-weight:600; color:#ff0000;\" >"
        con_text += str(doa_data[1])
        con_text += ("</span>")
        self.confidence_value.setText(con_text)
        
        
    
    def en_mapping_fun(self):
        if self.checkBox_en_mapping.checkState():
            self.en_mapping = 'True'
        else:
            self.en_mapping = 'False'
     
    def set_map_params(self):
        self.latitude = self.doubleSpinBox_latitude.value()
        self.longitude = self.doubleSpinBox_longitude.value()
        self.beacon_lat = self.doubleSpinBox_latitude_beacon.value()
        self.beacon_long = self.doubleSpinBox_longitude_beacon.value() 

        if self.initial != True:
            self.latitude = self.doubleSpinBox_latitude_new.value()
            self.longitude = self.doubleSpinBox_longitude_new.value()
        
        
    def set_default_configuration(self):
        self.checkBox_en_mapping.setChecked(False)
        self.doubleSpinBox_latitude.setProperty("value",35.30256)
        self.doubleSpinBox_longitude.setProperty("value",-120.66471)
        self.doubleSpinBox_latitude_beacon.setProperty("value",35.30287)
        self.doubleSpinBox_longitude_beacon.setProperty("value",-120.66427)
    
    def map_recenter(self):
        map = geotiler.Map(center=(self.longitude, self.latitude), zoom=19, size=(512, 512))
        self.map_bounds = map.extent
        image = geotiler.render_map(map)
        image.save('map.png')
        self.plot()
        self.initial = False  

    def tab_changed(self):
        tab_index = self.tabWidget.currentIndex()

        if tab_index == 0:  # Spectrum tab
            self.stackedWidget_config.setCurrentIndex(0)
        elif tab_index == 1:  # Sync tab
            self.stackedWidget_config.setCurrentIndex(1)
        elif tab_index == 2:  # DOA tab
            self.stackedWidget_config.setCurrentIndex(2)
        elif tab_index == 3:  # PR tab
            self.stackedWidget_config.setCurrentIndex(3)

    def run(self):
        if self.en_mapping:
            ### First, get the data from DOA calculations
            self.f = open("DOA_file.txt","r")
            self.data_buf = self.f.read().split("<")
            self.doa_buf = float(self.data_buf[0])
            print(self.doa)
            if abs(self.doa_buf-self.doa[self.i]) >= 0.1:
                print("here")
                if self.i == 2:
                    self.i = 0
                else:
                    self.i = self.i+1
                    
                self.doa[self.i] = self.doa_buf
                self.plot()
                self.DOA_update(self.data_buf)
            
    # def calc_distance(self):
        
        
        
        

#-----------------------------------------------------------------
#
#-----------------------------------------------------------------
def intersectLines( pt1, pt2, ptA, ptB ): 
    """ this returns the intersection of Line(pt1,pt2) and Line(ptA,ptB)
        
        returns a tuple: (xi, yi, valid, r, s), where
        (xi, yi) is the intersection
        r is the scalar multiple such that (xi,yi) = pt1 + r*(pt2-pt1)
        s is the scalar multiple such that (xi,yi) = pt1 + s*(ptB-ptA)
            valid == 0 if there are 0 or inf. intersections (invalid)
            valid == 1 if it has a unique intersection ON the segment    """

    DET_TOLERANCE = 0.00000001

    # the first line is pt1 + r*(pt2-pt1)
    # in component form:
    x1, y1 = pt1;   x2, y2 = pt2
    dx1 = x2 - x1;  dy1 = y2 - y1

    # the second line is ptA + s*(ptB-ptA)
    x, y = ptA;   xB, yB = ptB;
    dx = xB - x;  dy = yB - y;

    # we need to find the (typically unique) values of r and s
    # that will satisfy
    #
    # (x1, y1) + r(dx1, dy1) = (x, y) + s(dx, dy)
    #
    # which is the same as
    #
    #    [ dx1  -dx ][ r ] = [ x-x1 ]
    #    [ dy1  -dy ][ s ] = [ y-y1 ]
    #
    # whose solution is
    #
    #    [ r ] = _1_  [  -dy   dx ] [ x-x1 ]
    #    [ s ] = DET  [ -dy1  dx1 ] [ y-y1 ]
    #
    # where DET = (-dx1 * dy + dy1 * dx)
    #
    # if DET is too small, they're parallel
    #
    DET = (-dx1 * dy + dy1 * dx)

    if math.fabs(DET) < DET_TOLERANCE: return (0,0,0,0,0)

    # now, the determinant should be OK
    DETinv = 1.0/DET

    # find the scalar amount along the "self" segment
    r = DETinv * (-dy  * (x-x1) +  dx * (y-y1))

    # find the scalar amount along the input line
    s = DETinv * (-dy1 * (x-x1) + dx1 * (y-y1))

    # return the average of the two descriptions
    xi = (x1 + r*dx1 + x + s*dx)/2.0
    yi = (y1 + r*dy1 + y + s*dy)/2.0
    return ( xi, yi, 1, r, s )	


app = QApplication(sys.argv)
form = MainWindow()
form.show()
app.exec_()
