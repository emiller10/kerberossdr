import sys
import os
import time
import math
import pyqtgraph as pg
import pyqtgraph.exporters
import numpy as np
import scipy
from bottle import route, run, request, get, post, redirect, template, static_file
import threading
import subprocess
import save_settings as settings

np.seterr(divide='ignore')

# Import Kerberos modules
currentPath = os.path.dirname(os.path.realpath(__file__))
rootPath = os.path.dirname(currentPath)

receiverPath        = os.path.join(rootPath, "_receiver")
signalProcessorPath = os.path.join(rootPath, "_signalProcessing")

sys.path.insert(0, receiverPath)
sys.path.insert(0, signalProcessorPath)

from hydra_receiver import ReceiverRTLSDR

# Import graphical user interface packages --Modified to use PyQt5
from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import *
from PyQt5.QtGui import *

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

# Import packages for plotting
import matplotlib
matplotlib.use('Agg') # For Raspberry Pi compatiblity
from matplotlib import cm
#from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
#from matplotlib.backends.backend_qt4 import NavigationToolbar2QT as NavigationToolbar
#import matplotlib.pyplot as plt
#import matplotlib.patches as patches


from hydra_main_window_layout import Ui_MainWindow
from hydra_signal_processor import SignalProcessor

# Import the pyArgus module
#root_path = os.getcwd()
#pyargus_path = os.path.join(os.path.join(root_path, "pyArgus"), "pyArgus")
#sys.path.insert(0, pyargus_path)
#import directionEstimation as de

from pyargus import directionEstimation as de

class MainWindow(QMainWindow, Ui_MainWindow):

    def __init__ (self,parent = None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        #f = open('/dev/null', 'w')
        #sys.stdout = f

        self.tabWidget.setCurrentIndex(0)
        
init_settings()
app.exec_()        