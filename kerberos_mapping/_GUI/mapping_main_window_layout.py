# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Mapping_hydra_dev.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1069, 867)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.stackedWidget_config = QtWidgets.QStackedWidget(self.centralwidget)
        self.stackedWidget_config.setMaximumSize(QtCore.QSize(290, 16777215))
        self.stackedWidget_config.setObjectName("stackedWidget_config")
        self.page_2 = QtWidgets.QWidget()
        self.page_2.setObjectName("page_2")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.page_2)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox_4 = QtWidgets.QGroupBox(self.page_2)
        self.groupBox_4.setObjectName("groupBox_4")
        self.gridLayout_10 = QtWidgets.QGridLayout(self.groupBox_4)
        self.gridLayout_10.setObjectName("gridLayout_10")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.pushButton_update_beacon = QtWidgets.QPushButton(self.groupBox_4)
        self.pushButton_update_beacon.setObjectName("pushButton_update_beacon")
        self.gridLayout.addWidget(self.pushButton_update_beacon, 9, 0, 1, 2)
        self.label_DOA_title = QtWidgets.QLabel(self.groupBox_4)
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        self.label_DOA_title.setFont(font)
        self.label_DOA_title.setObjectName("label_DOA_title")
        self.gridLayout.addWidget(self.label_DOA_title, 13, 0, 1, 2)
        self.doubleSpinBox_latitude = QtWidgets.QDoubleSpinBox(self.groupBox_4)
        self.doubleSpinBox_latitude.setDecimals(5)
        self.doubleSpinBox_latitude.setMinimum(-360.0)
        self.doubleSpinBox_latitude.setMaximum(360.0)
        self.doubleSpinBox_latitude.setObjectName("doubleSpinBox_latitude")
        self.gridLayout.addWidget(self.doubleSpinBox_latitude, 2, 1, 1, 1)
        self.label_long_3 = QtWidgets.QLabel(self.groupBox_4)
        self.label_long_3.setObjectName("label_long_3")
        self.gridLayout.addWidget(self.label_long_3, 12, 0, 1, 1)
        self.label_DOA = QtWidgets.QLabel(self.groupBox_4)
        self.label_DOA.setObjectName("label_DOA")
        self.gridLayout.addWidget(self.label_DOA, 14, 0, 1, 1)
        self.label_GPS = QtWidgets.QLabel(self.groupBox_4)
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        self.label_GPS.setFont(font)
        self.label_GPS.setObjectName("label_GPS")
        self.gridLayout.addWidget(self.label_GPS, 1, 0, 1, 2)
        self.label_distance = QtWidgets.QLabel(self.groupBox_4)
        self.label_distance.setObjectName("label_distance")
        self.gridLayout.addWidget(self.label_distance, 15, 0, 1, 1)
        self.label_beacon_location = QtWidgets.QLabel(self.groupBox_4)
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        self.label_beacon_location.setFont(font)
        self.label_beacon_location.setObjectName("label_beacon_location")
        self.gridLayout.addWidget(self.label_beacon_location, 5, 0, 1, 2)
        self.doubleSpinBox_longitude = QtWidgets.QDoubleSpinBox(self.groupBox_4)
        self.doubleSpinBox_longitude.setDecimals(5)
        self.doubleSpinBox_longitude.setMinimum(-360.0)
        self.doubleSpinBox_longitude.setMaximum(360.0)
        self.doubleSpinBox_longitude.setObjectName("doubleSpinBox_longitude")
        self.gridLayout.addWidget(self.doubleSpinBox_longitude, 3, 1, 1, 1)
        self.confidence_value = QtWidgets.QLabel(self.groupBox_4)
        self.confidence_value.setObjectName("confidence_value")
        self.gridLayout.addWidget(self.confidence_value, 16, 1, 1, 1)
        self.label_DOA_enable = QtWidgets.QLabel(self.groupBox_4)
        self.label_DOA_enable.setObjectName("label_DOA_enable")
        self.gridLayout.addWidget(self.label_DOA_enable, 0, 0, 1, 1)
        self.label_long = QtWidgets.QLabel(self.groupBox_4)
        self.label_long.setObjectName("label_long")
        self.gridLayout.addWidget(self.label_long, 3, 0, 1, 1)
        self.label_latitude_beacon = QtWidgets.QLabel(self.groupBox_4)
        self.label_latitude_beacon.setObjectName("label_latitude_beacon")
        self.gridLayout.addWidget(self.label_latitude_beacon, 6, 0, 1, 1)
        self.pushButton_recenter = QtWidgets.QPushButton(self.groupBox_4)
        self.pushButton_recenter.setObjectName("pushButton_recenter")
        self.gridLayout.addWidget(self.pushButton_recenter, 4, 0, 1, 2)
        self.label_lat = QtWidgets.QLabel(self.groupBox_4)
        self.label_lat.setObjectName("label_lat")
        self.gridLayout.addWidget(self.label_lat, 2, 0, 1, 1)
        self.checkBox_en_mapping = QtWidgets.QCheckBox(self.groupBox_4)
        self.checkBox_en_mapping.setText("")
        self.checkBox_en_mapping.setChecked(True)
        self.checkBox_en_mapping.setObjectName("checkBox_en_mapping")
        self.gridLayout.addWidget(self.checkBox_en_mapping, 0, 1, 1, 1)
        self.label_longitude_beacon = QtWidgets.QLabel(self.groupBox_4)
        self.label_longitude_beacon.setObjectName("label_longitude_beacon")
        self.gridLayout.addWidget(self.label_longitude_beacon, 7, 0, 1, 1)
        self.doubleSpinBox_latitude_new = QtWidgets.QDoubleSpinBox(self.groupBox_4)
        self.doubleSpinBox_latitude_new.setDecimals(5)
        self.doubleSpinBox_latitude_new.setMinimum(-360.0)
        self.doubleSpinBox_latitude_new.setMaximum(360.0)
        self.doubleSpinBox_latitude_new.setObjectName("doubleSpinBox_latitude_new")
        self.gridLayout.addWidget(self.doubleSpinBox_latitude_new, 11, 1, 1, 1)
        self.doubleSpinBox_longitude_beacon = QtWidgets.QDoubleSpinBox(self.groupBox_4)
        self.doubleSpinBox_longitude_beacon.setDecimals(5)
        self.doubleSpinBox_longitude_beacon.setMinimum(-360.0)
        self.doubleSpinBox_longitude_beacon.setMaximum(360.0)
        self.doubleSpinBox_longitude_beacon.setObjectName("doubleSpinBox_longitude_beacon")
        self.gridLayout.addWidget(self.doubleSpinBox_longitude_beacon, 7, 1, 1, 1)
        self.label_confidence = QtWidgets.QLabel(self.groupBox_4)
        self.label_confidence.setObjectName("label_confidence")
        self.gridLayout.addWidget(self.label_confidence, 16, 0, 1, 1)
        self.distance_value = QtWidgets.QLabel(self.groupBox_4)
        self.distance_value.setObjectName("distance_value")
        self.gridLayout.addWidget(self.distance_value, 15, 1, 1, 1)
        self.label_lat_3 = QtWidgets.QLabel(self.groupBox_4)
        self.label_lat_3.setObjectName("label_lat_3")
        self.gridLayout.addWidget(self.label_lat_3, 11, 0, 1, 1)
        self.doubleSpinBox_latitude_beacon = QtWidgets.QDoubleSpinBox(self.groupBox_4)
        self.doubleSpinBox_latitude_beacon.setDecimals(5)
        self.doubleSpinBox_latitude_beacon.setMinimum(-360.0)
        self.doubleSpinBox_latitude_beacon.setMaximum(360.0)
        self.doubleSpinBox_latitude_beacon.setObjectName("doubleSpinBox_latitude_beacon")
        self.gridLayout.addWidget(self.doubleSpinBox_latitude_beacon, 6, 1, 1, 1)
        self.DOA_value = QtWidgets.QLabel(self.groupBox_4)
        self.DOA_value.setObjectName("DOA_value")
        self.gridLayout.addWidget(self.DOA_value, 14, 1, 1, 1)
        self.doubleSpinBox_longitude_new = QtWidgets.QDoubleSpinBox(self.groupBox_4)
        self.doubleSpinBox_longitude_new.setDecimals(5)
        self.doubleSpinBox_longitude_new.setMinimum(-360.0)
        self.doubleSpinBox_longitude_new.setMaximum(360.0)
        self.doubleSpinBox_longitude_new.setObjectName("doubleSpinBox_longitude_new")
        self.gridLayout.addWidget(self.doubleSpinBox_longitude_new, 12, 1, 1, 1)
        self.pushButton_run = QtWidgets.QPushButton(self.groupBox_4)
        self.pushButton_run.setObjectName("pushButton_run")
        self.gridLayout.addWidget(self.pushButton_run, 17, 0, 1, 2)
        self.label_GPS_3 = QtWidgets.QLabel(self.groupBox_4)
        font = QtGui.QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        self.label_GPS_3.setFont(font)
        self.label_GPS_3.setObjectName("label_GPS_3")
        self.gridLayout.addWidget(self.label_GPS_3, 10, 0, 1, 2)
        self.gridLayout_10.addLayout(self.gridLayout, 0, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_10.addItem(spacerItem, 1, 0, 1, 1)
        self.verticalLayout.addWidget(self.groupBox_4)
        self.stackedWidget_config.addWidget(self.page_2)
        self.verticalLayout_6.addWidget(self.stackedWidget_config)
        self.horizontalLayout.addLayout(self.verticalLayout_6)
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_mapping = QtWidgets.QWidget()
        self.tab_mapping.setObjectName("tab_mapping")
        self.gridLayout_23 = QtWidgets.QGridLayout(self.tab_mapping)
        self.gridLayout_23.setObjectName("gridLayout_23")
        self.gridLayout_RD = QtWidgets.QGridLayout()
        self.gridLayout_RD.setObjectName("gridLayout_RD")
        self.gridLayout_23.addLayout(self.gridLayout_RD, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_mapping, "")
        self.horizontalLayout.addWidget(self.tabWidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1069, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.stackedWidget_config.setCurrentIndex(0)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.groupBox_4.setTitle(_translate("MainWindow", "Mapping"))
        self.pushButton_update_beacon.setText(_translate("MainWindow", "Update beacon location"))
        self.label_DOA_title.setText(_translate("MainWindow", "Direction of Arrival:"))
        self.label_long_3.setText(_translate("MainWindow", "Longitude:"))
        self.label_DOA.setText(_translate("MainWindow", "DOA:"))
        self.label_GPS.setText(_translate("MainWindow", "GPS Coordinates:"))
        self.label_distance.setText(_translate("MainWindow", "Distance from Beacon:"))
        self.label_beacon_location.setText(_translate("MainWindow", "Beacon Location:"))
        self.confidence_value.setText(_translate("MainWindow", "- %"))
        self.label_DOA_enable.setText(_translate("MainWindow", "Enable/Disable:"))
        self.label_long.setText(_translate("MainWindow", "Longitude:"))
        self.label_latitude_beacon.setText(_translate("MainWindow", "Latitude:"))
        self.pushButton_recenter.setText(_translate("MainWindow", "Recenter Map"))
        self.label_lat.setText(_translate("MainWindow", "Latitude:"))
        self.label_longitude_beacon.setText(_translate("MainWindow", "Longitude"))
        self.label_confidence.setText(_translate("MainWindow", "Confidence"))
        self.distance_value.setText(_translate("MainWindow", "- meters"))
        self.label_lat_3.setText(_translate("MainWindow", "Latitude:"))
        self.DOA_value.setText(_translate("MainWindow", "- degrees"))
        self.pushButton_run.setText(_translate("MainWindow", "Run"))
        self.label_GPS_3.setText(_translate("MainWindow", "New Coordinates:"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_mapping), _translate("MainWindow", "Mapping"))
